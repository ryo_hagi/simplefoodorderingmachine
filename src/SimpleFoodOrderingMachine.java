import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton KatudonButton;
    private JButton OyakodonButton;
    private JButton GyudonButton;
    private JButton checkout;
    private JTextArea NameAndPrice;
    private JLabel Ordered;
    private JButton TendonButton;
    private JButton KaisendonButton;
    private JButton ButadonButton;
    private JRadioButton eatinRadioButton;
    private JRadioButton takeoutRadioButton;
    private JLabel totalLabel;
    private JButton Cancelbutton;


    private static int foodcost(String food){
        int cost=0;
        switch (food){
            case "Gyudon":    cost=400;break;
            case "Katudon":   cost=600;break;
            case "Oyakodon":  cost=700;break;
            case "Tendon":    cost=1000;break;
            case "Kaisendon": cost=1200;break;
            case "Butadon":   cost=450;break;
        }

        return cost;

    }

    int sum=0;
    int tax=0;
    int taxsum=0;
    int total=0;


    void order(String food){

        int confirmation = JOptionPane.showConfirmDialog(null,
                " would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation==1){

        } else{
            if(takeoutRadioButton.isSelected()){
            tax=(int)(foodcost(food)*0.08);
            taxsum+=tax;
                String orderItemList = NameAndPrice.getText();
                NameAndPrice.setText(orderItemList +"(to go)"+ food + " " +(foodcost(food)+tax) + "yen\n");
            }
            else if(eatinRadioButton.isSelected()){
            tax=(int)(foodcost(food)*0.1);
            taxsum+=tax;
                String orderItemList = NameAndPrice.getText();
                NameAndPrice.setText(orderItemList +"(for here)"+ food + " " + (foodcost(food)+tax) + "yen\n");
            }
            else{
            JOptionPane.showMessageDialog(null, "Select for here or to go.");
            confirmation=1;
            }
            if (confirmation == 0) {


                JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! \nIt will be served as soon as possible.\nSubtotal:"+foodcost(food)+"yen\ntax:"+tax+"yen\ntotal:"+(foodcost(food)+tax)+"yen");
                sum += foodcost(food);
                total = sum + taxsum;



                totalLabel.setText("total    " + total + "yen");
            }


        }
    }




    public SimpleFoodOrderingMachine() {
        ButtonGroup bgroup = new ButtonGroup();
        bgroup.add(takeoutRadioButton);
        bgroup.add(eatinRadioButton);


        totalLabel.setText("total    " + total + "yen");




        GyudonButton.setIcon(new ImageIcon(
                this.getClass().getResource("gyudon.jpg")));
        KatudonButton.setIcon(new ImageIcon(
                this.getClass().getResource("katudon.jpg")));
        OyakodonButton.setIcon(new ImageIcon(
                this.getClass().getResource("oyakodon.jpg")));
        TendonButton.setIcon(new ImageIcon(
                this.getClass().getResource("tendon.jpg")));
        KaisendonButton.setIcon(new ImageIcon(
                this.getClass().getResource("kaisendon.jpg")));
        ButadonButton.setIcon(new ImageIcon(
                this.getClass().getResource("butadon.jpg")));
            GyudonButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Gyudon");
                }
            });
            KatudonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Katudon");
                }
            });

            OyakodonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    order("Oyakodon");
                }
            });


            checkout.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to check out?",
                            "Check out",
                            JOptionPane.YES_NO_OPTION);

                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(null, "Thank you! The total praice is " + total + "yen.");
                        NameAndPrice.setText("");
                        sum = 0;
                        tax=0;
                        total=0;
                        taxsum=0;


                        totalLabel.setText("total    " + total + "yen");
                        bgroup.clearSelection();

                    }

                }

            });



            TendonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    order("Tendon");
                }
            });
            KaisendonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    order("Kaisendon");
                }
            });
            ButadonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    order("Butadon");
                }
            });

        Cancelbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel?",
                        "Cancel",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "cancel");
                    NameAndPrice.setText("");
                    sum = 0;
                    tax = 0;
                    total = 0;
                    taxsum = 0;


                    totalLabel.setText("total    " + total + "yen");
                    bgroup.clearSelection();
                }
            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
